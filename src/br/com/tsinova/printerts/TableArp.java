package br.com.tsinova.printerts;

import java.util.List;

public class TableArp {
    
    private String oid;
    private List<Host> hosts;

    public TableArp(String oid, List<Host> hosts) {
        this.oid = oid;
        this.hosts = hosts;
    }

    public TableArp() {
    }
    
    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public List<Host> getHosts() {
        return hosts;
    }

    public void setHosts(List<Host> hosts) {
        this.hosts = hosts;
    }
    
}
