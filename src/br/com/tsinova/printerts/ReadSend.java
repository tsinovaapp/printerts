package br.com.tsinova.printerts;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.DefaultPDUFactory;
import org.snmp4j.util.TreeEvent;
import org.snmp4j.util.TreeUtils;

public class ReadSend extends Thread {

    private final Printer printer;
    private final Data data;
    private final List<Output> listOutput;
    private boolean run;
    private final Beat beat;

    public ReadSend(Printer switche, Data data, List<Output> listOutput, Beat beat) {
        this.printer = switche;
        this.data = data;
        this.listOutput = listOutput;
        this.run = true;
        this.beat = beat;
    }

    public void close() {
        this.run = false;
    }

    private List<Oid> getLisOidByType(List<Oid> list, int... types) {
        List<Oid> newList = new ArrayList<>();
        for (Oid oid : list) {
            if (!oid.isEnabled()) {
                continue;
            }
            for (int i = 0; i < types.length; i++) {
                if (oid.getType() == types[i]) {
                    newList.add(oid);
                    break;
                }
            }
        }
        return newList;
    }

    private CommunityTarget getTarget(Host host) {
        CommunityTarget target = new CommunityTarget();
        if (host.getVersion().equals("2c")) {
            target.setCommunity(new OctetString(host.getCommunity()));
            target.setAddress(GenericAddress.parse("udp:" + host.getHost() + "/" + host.getPort()));
            target.setRetries(2);
            target.setTimeout(1500);
            target.setVersion(SnmpConstants.version2c);
        } else if (host.getVersion().equals("1")) {
            target.setCommunity(new OctetString(host.getCommunity()));
            target.setAddress(GenericAddress.parse("udp:" + host.getHost() + "/" + host.getPort()));
            target.setRetries(2);
            target.setTimeout(1500);
            target.setVersion(SnmpConstants.version1);
        }
        return target;
    }

    private PDU getPDU(List<Oid> listOids, String add) {
        PDU pdu = new PDU();
        pdu.setType(PDU.GET);
        listOids.forEach((oid) -> {
            pdu.add(new VariableBinding(new OID("." + oid.getOid() + add)));
        });
        return pdu;
    }

    /**
     * Converte o valor armazenado em um OID em um tipo específico
     *
     * @param type
     * @param variableBinding
     * @return
     * @throws Exception
     */
    private Object getValueOID(int type, VariableBinding variableBinding) throws Exception {
        switch (type) {
            case Oid.INTEGER:
                // inteiro
                return variableBinding.getVariable().toInt();
            case Oid.TEXT:
                // texto
                return variableBinding.getVariable().toString();
            case Oid.LONG:
                // Long
                return variableBinding.getVariable().toLong();
            case Oid.DOUBLE:
                // Double
                return Double.parseDouble(variableBinding.getVariable().toString());
            case Oid.ERROR_STATE:
                // texto
                return variableBinding.getVariable().toString();
            default:
                break;
        }
        throw new Exception("Invalid variable type");
    }

    /**
     * Retorna o valor armazenado em um OID de um host
     *
     * @param target
     * @param snmp
     * @param listOidsPort
     * @param add
     * @return
     * @throws Exception
     */
    private JSONObject getResponseOidsSimplePort(CommunityTarget target, Snmp snmp, List<Oid> listOidsPort, String add, Integer port) throws Exception {

        List<Oid> listOidsSimple = getLisOidByType(listOidsPort, Oid.INTEGER,
                Oid.TEXT, Oid.LONG, Oid.DOUBLE, Oid.COUNTER_REGISTER, Oid.ERROR_STATE);

        ResponseEvent responseEvent = snmp.send(getPDU(listOidsSimple, add), target);
        PDU response = responseEvent.getResponse();

        JSONObject json = new JSONObject();

        for (Oid oid : listOidsSimple) {
            boolean flag = false;

            for (int i = 0; i < response.size(); i++) {

                VariableBinding variableBinding = response.get(i);

                if (variableBinding.getOid().toString().equalsIgnoreCase(oid.getOid() + add)) {

                    if (variableBinding.isException() || variableBinding.getVariable().isException()
                            || !variableBinding.getOid().isValid() || variableBinding.getOid().isException()) {
                        json.put(oid.getName(), oid.getValueDefault());

                    } else if (oid.isUseDefaultIfExists()) {
                        json.put(oid.getName(), oid.getDefaultIfExists());

                    } else {

                        if (oid.isUseJoinOid()) {
                            String join_id = oid.getJoinOid() + "." + variableBinding.getVariable().toString();
                            Oid newOid = new Oid();
                            newOid.setName(oid.getName());
                            newOid.setOid(join_id);
                            newOid.setType(2);
                            newOid.setValueDefault("");
                            JSONObject jsonJsonId = getResponseOidsSimple(target, Arrays.asList(newOid), "", 0);
                            json.put(oid.getName(), jsonJsonId.get(oid.getName()));

                        } else {

                            Object value;

                            if (oid.getType() == Oid.ERROR_STATE){
                                value = Util.getErrorState(getValueOID(oid.getType(), 
                                        variableBinding).toString());
                                
                            }else{
                                value = getValueOID(oid.getType(), variableBinding);
                                
                            }
                            
                            
                            if (oid.getScript() != null && !oid.getScript().isEmpty()) {
                                value = Util.executeScript(oid.getScript(), value);

                            }

                            json.put(oid.getName(), value);

                        }

                    }

                    flag = true;
                    break;

                }

            }

            if (!flag) {
                json.put(oid.getName(), oid.getValueDefault());
            }

        }

        return json;

    }

    /**
     * Retorna um valor armazenado em um OID de um host
     *
     * @param target
     * @param listOids
     * @param add
     * @return
     * @throws Exception
     */
    private JSONObject getResponseOidsSimple(CommunityTarget target, List<Oid> listOids, String add, Integer port) throws Exception {
        TransportMapping transport = new DefaultUdpTransportMapping();
        Snmp snmp = new Snmp(transport);
        transport.listen();

        // leitura de variavel simples da porta
        JSONObject jsonResponseOidsSimple = getResponseOidsSimplePort(target, snmp, listOids, add, port);

        snmp.close();
        transport.close();

        return jsonResponseOidsSimple;

    }

    /**
     * Retorna a quantidade de registros de um OID de um host
     *
     * @param target
     * @param snmp
     * @param oid
     * @return
     */
    private int getCounterByOid(CommunityTarget target, Snmp snmp, Oid oid) {
        TreeUtils treeUtils = new TreeUtils(snmp, new DefaultPDUFactory());
        List<TreeEvent> events = treeUtils.getSubtree(target, new OID("." + oid.getOid()));
        int counter = 0;
        for (TreeEvent event : events) {
            if (event == null) {
                continue;
            }
            if (event.isError()) {
                continue;
            }
            VariableBinding[] varBindings = event.getVariableBindings();
            for (VariableBinding varBinding : varBindings) {
                if (varBinding == null) {
                    continue;
                }
                counter++;
            }
        }
        return counter;

    }

    private JSONObject getResponseOidsCounter(CommunityTarget target, List<Oid> oids) throws Exception {
        TransportMapping transport = new DefaultUdpTransportMapping();
        Snmp snmp = new Snmp(transport);
        transport.listen();

        JSONObject json = new JSONObject();

        for (Oid oid : oids) {
            int counter = getCounterByOid(target, snmp, oid);
            json.put(oid.getName(), counter);
        }

        snmp.close();
        transport.close();

        return json;
    }

    private JSONObject getJson(JSONObject jsonValuesDefault, JSONObject jsonValues) throws JSONException {

        JSONObject json = new JSONObject();

        Iterator it = jsonValuesDefault.keys();
        while (it.hasNext()) {
            String key = it.next().toString();
            json.put(key, jsonValuesDefault.get(key));
        }

        it = jsonValues.keys();
        while (it.hasNext()) {
            String key = it.next().toString();
            json.put(key, jsonValues.get(key));
        }

        return json;
    }

    @Override
    public void run() {

        while (run) {

            // efetuando a leitura de oids simples
            System.out.println("Reading the Printer OIDs " + printer.getHost() + ", Interval " + data.getInterval() + "s");

            JSONObject json = new JSONObject();

            CommunityTarget target = getTarget(printer);

            // Leitura de oids simples / OID = Value            
            List<Oid> listOidsSimple = getLisOidByType(data.getListOid(), Oid.INTEGER,
                    Oid.LONG, Oid.TEXT, Oid.DOUBLE, Oid.ERROR_STATE);
            if (!listOidsSimple.isEmpty()) {
                try {
                    json = getResponseOidsSimple(target, listOidsSimple, "", null);
                } catch (Exception ex) {
                    for (Oid oid : listOidsSimple) {
                        try {
                            json.put(oid.getName(), oid.getValueDefault());
                        } catch (JSONException ex1) {
                        }
                    }
                }
            }

            // Efetuando a leitura de Oids contadores
            List<Oid> listOidsCounter = getLisOidByType(data.getListOid(), Oid.COUNTER_REGISTER);
            if (!listOidsCounter.isEmpty()) {
                try {
                    JSONObject responseOidsCounter = getResponseOidsCounter(target, listOidsCounter);
                    for (Oid oid : listOidsCounter) {
                        json.put(oid.getName(), responseOidsCounter.get(oid.getName()));
                    }
                } catch (Exception ex) {
                    for (Oid oid : listOidsCounter) {
                        try {
                            json.put(oid.getName(), oid.getValueDefault());
                        } catch (JSONException ex1) {
                        }
                    }
                }
            }

            // Seta outros atributos, atributos que qualquer documento tem
            JSONObject jsonDefault = new JSONObject();

            try {

                JSONObject deviceJSON = new JSONObject();
                deviceJSON.put("ip", printer.getHost());
                jsonDefault.put("device", deviceJSON);

                jsonDefault.put("timestamp", Util.getDatetimeNowForElasticsearch());

                JSONObject metadataJSON = new JSONObject();
                metadataJSON.put("beat", beat.getName());
                metadataJSON.put("version", beat.getVersion());
                jsonDefault.put("@metadata", metadataJSON);

                JSONObject beatJSON = new JSONObject();
                beatJSON.put("name", beat.getName());
                beatJSON.put("version", beat.getVersion());
                jsonDefault.put("beat", beatJSON);

                jsonDefault.put("tags", beat.getTags());

            } catch (JSONException ex) {
            }

            System.out.println("Data Printer: " + json.toString());

            try {

                for (Output out : listOutput) {

                    if ((data.getAction() == null
                            || data.getAction().equalsIgnoreCase("append")
                            || data.getAction().equalsIgnoreCase("append_and_update_last"))
                            && json.length() > 0) {
                        // add type
                        if (data.getType() != null && !data.getType().isEmpty()) {
                            try {
                                json.put("type", data.getType());
                            } catch (JSONException ex) {
                            }
                        }
                        try (Socket socket = new Socket(out.host, out.port);
                                DataOutputStream os = new DataOutputStream(
                                        new BufferedOutputStream(socket.getOutputStream()))) {
                            os.writeBytes(getJson(jsonDefault, json).toString());
                            os.flush();
                        }
                        System.out.println("Data sent to " + out.host + ":" + out.port);
                    }

                    if (data.getAction() != null && (data.getAction().equalsIgnoreCase("update_last")
                            || data.getAction().equalsIgnoreCase("append_and_update_last"))
                            && data.getIdDocActionUpdateLast() != null
                            && json.length() > 0) {
                        // add type
                        if (data.getType() != null && !data.getType().isEmpty()) {
                            try {
                                json.put("type", "Latest" + data.getType());
                            } catch (JSONException ex) {
                            }
                        }
                        json.put("id_doc", Util.getIdDoc(data.getIdDocActionUpdateLast(), json));
                        try (Socket socket = new Socket(out.host, out.port);
                                DataOutputStream os = new DataOutputStream(
                                        new BufferedOutputStream(socket.getOutputStream()))) {
                            os.writeBytes(getJson(jsonDefault, json).toString());
                            os.flush();
                        }
                        System.out.println("Data sent to " + out.host + ":" + out.port);
                    }

                }

            } catch (Exception ex) {
            }

            // Intervalo de espera
            try {
                Thread.sleep(data.getInterval() * 1000);
            } catch (InterruptedException ex) {
            }

        }

    }

}
