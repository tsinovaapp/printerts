package br.com.tsinova.printerts;

import org.json.JSONException;
import org.json.JSONObject;

public class Sum {
    
    private String name;
    private String oidName;
    private Object valueDefault;
    private String script;
    private boolean enabled;

    public Sum(String name, String oidName, Object valueDefault, String script) {
        this.name = name;
        this.oidName = oidName;
        this.valueDefault = valueDefault;
        this.script = script;
    }

    public Sum() {
    }
    
    public Sum(JSONObject obj) throws JSONException {
        this.name = obj.getString("name");
        this.oidName = obj.getString("oid_name");
        this.valueDefault = obj.get("default");
        this.enabled = true;
        if (obj.has("script")){
            this.script = obj.getString("script");                
        }
        if (obj.has("enabled")){
            this.enabled = obj.getInt("enabled") == 1;                
        }
    }    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOidName() {
        return oidName;
    }

    public void setOidName(String oidName) {
        this.oidName = oidName;
    }

    public Object getValueDefault() {
        return valueDefault;
    }

    public void setValueDefault(Object valueDefault) {
        this.valueDefault = valueDefault;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
            
    
}
