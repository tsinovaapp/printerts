package br.com.tsinova.printerts;

import java.util.List;

public class Data {
    
    private int interval;
    private String type;
    private String action;
    private String idDocActionUpdateLast;
    private List<Oid> listOid; 

    public Data(int interval, List<Oid> listOid) {
        this.interval = interval;
        this.listOid = listOid;
    }

    public Data() {
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public List<Oid> getListOid() {
        return listOid;
    }

    public void setListOid(List<Oid> listOid) {
        this.listOid = listOid;
    }    

    @Override
    public String toString() {
        return "Data{" + "interval=" + interval + ", listOid=" + listOid + '}';
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getIdDocActionUpdateLast() {
        return idDocActionUpdateLast;
    }

    public void setIdDocActionUpdateLast(String idDocActionUpdateLast) {
        this.idDocActionUpdateLast = idDocActionUpdateLast;
    }    
    
}
