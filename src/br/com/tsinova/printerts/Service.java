package br.com.tsinova.printerts;

import java.io.FileInputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Service extends Thread{
    
    private final List<ReadSend> listReadSends;

    public Service() {
        listReadSends = new ArrayList<>();
    }    
    
    public List<Printer> getListPrinters(JSONObject json) throws Exception{
        
        List<Printer> listPrinter = new ArrayList<>();
            
            JSONArray arrayNobreaks = json.getJSONArray("printers");
            
            for (int i = 0; i < arrayNobreaks.length(); i++) {
                
                JSONObject nobreakJSON = arrayNobreaks.getJSONObject(i);
                
                Printer nobreak = new Printer();
                nobreak.setHost(nobreakJSON.getString("host"));
                nobreak.setPort(nobreakJSON.getInt("port"));
                nobreak.setVersion(nobreakJSON.getString("version"));
                nobreak.setCommunity(nobreakJSON.getString("community"));
                
                JSONArray listDataJSON = nobreakJSON.getJSONArray("data"); 
                List<Data> listData = new ArrayList<>();
                
                for (int j = 0; j < listDataJSON.length(); j++) {
                    
                    JSONObject dataJSON = listDataJSON.getJSONObject(j);
                    
                    Data data = new Data();
                    data.setInterval(dataJSON.getInt("interval"));
                    
                    if (dataJSON.has("type")){
                        data.setType(dataJSON.getString("type"));
                    }                    
                    if (dataJSON.has("action")){
                        data.setAction(dataJSON.getString("action"));
                    }                    
                    if (dataJSON.has("id_doc_action_update_last")){
                        data.setIdDocActionUpdateLast(dataJSON.getString("id_doc_action_update_last"));
                    }
                    
                    JSONArray listOidsJSON = dataJSON.getJSONArray("oids");
                    
                    List<Oid> listOids = new ArrayList<>();
                    
                    for (int k = 0; k < listOidsJSON.length(); k++) {
                        
                        JSONObject oidJSON = listOidsJSON.getJSONObject(k);
                        
                        Oid oid = new Oid();
                        oid.setName(oidJSON.getString("name"));
                        oid.setOid(oidJSON.getString("oid"));
                        oid.setType(oidJSON.getInt("type"));                        
                        if (oidJSON.has("default")){
                            oid.setValueDefault(oidJSON.get("default")); 
                        }
                        if (oidJSON.has("script")){
                            oid.setScript(oidJSON.getString("script")); 
                        }
                        if (oidJSON.has("default_if_exists")){
                           oid.setUseDefaultIfExists(true);
                           oid.setDefaultIfExists(oidJSON.get("default_if_exists"));
                        }
                        if (oidJSON.has("join_oid")){
                           oid.setUseJoinOid(true);
                           oid.setJoinOid(oidJSON.getString("join_oid"));
                        }                        
                        if (oidJSON.has("add_doc_devices")){
                           oid.setAddDocDevices((oidJSON.getInt("add_doc_devices") == 1));
                        }
                        if (oidJSON.has("enabled")){
                           oid.setEnabled((oidJSON.getInt("enabled") == 1));
                        }                                                                                                                                               
                        
                        listOids.add(oid);
                        
                    }                    
                    
                    data.setListOid(listOids);
                    listData.add(data);
                    
                }
                
                nobreak.setListData(listData);
                listPrinter.add(nobreak);
                            
            }
            
            return listPrinter;
        
    }
    
    
    private Beat getBeat(JSONObject json) throws Exception{
        JSONObject beatJSON = json.getJSONObject("beat");
        Beat beat = new Beat();
        beat.setName(beatJSON.getString("name"));
        beat.setVersion(beatJSON.getString("version"));
        beat.setTags(beatJSON.getJSONArray("tags"));
        if (beatJSON.has("password_sudo")){
            beat.setPasswordSudo(beatJSON.getString("password_sudo"));
        }
        return beat;
    }
    
    public List<Output> getListOutputs(JSONObject json) throws Exception{
        
        JSONObject outputJSON = json.getJSONObject("output");
        List<Output> listOutputs = new ArrayList<>();
        
        if (outputJSON.has("logstash")){
            OutputLogstash outputLogstash = new OutputLogstash();
            outputLogstash.setHost(outputJSON.getJSONObject("logstash").getString("host"));
            outputLogstash.setPort(outputJSON.getJSONObject("logstash").getInt("port"));
            listOutputs.add(outputLogstash);
        }
        
        return listOutputs;        
        
    }

    @Override
    public void run() {
        
         // Efetua a leitura dos parâmetros ...
         
        System.out.println("Reading parameters ...");
        
        List<Printer> listPrinters;
        List<Output> listOutputs;
        Beat beat;
        
        try {
            JSONObject json = new JSONObject(new JSONTokener(new FileInputStream(Paths.get("printerts.json").toFile())));
            
            listPrinters = getListPrinters(json);
            listOutputs = getListOutputs(json);  
            beat = getBeat(json);
            
            System.out.println("Successful reading");
            
        } catch (Exception ex) {
            System.err.println("Failed to read");
            ex.printStackTrace();
            return;
        
        }        
        
        
        // inicia processos
        
        System.out.println("Starting reading and sending processes ...");
        
        for(Printer switche : listPrinters){            
            for(Data data : switche.getListData()){               
                ReadSend readSend = new ReadSend(switche, data, listOutputs, beat);
                readSend.start();
                listReadSends.add(readSend);                                
            }            
        }        
        
        System.out.println("Read and send processes successfully started");
        
        
    }
    
   
    
    public static void main(String[] args) {        
        Service service = new Service();
        service.start();                
    }
    
}
