package br.com.tsinova.printerts;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.Iterator;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.json.JSONArray;
import org.json.JSONObject;

public class Util {    
 
    private static final String STATUS[] = new String[]{"Pouco papel",
            "Sem papel",
            "Pouco toner",
            "Sem toner",
            "Porta aberta",
            "Papel encravado",
            "Desligada",
            "Serviço solicitado",
            "Bandeja de entrada ausente",
            "Bandeja de saída ausente",
            "Suprimento de marcador ausente",
            "Saída quase completa",
            "Saída completa",
            "Bandeja de entrada vazia",
            "Manutenção preventiva atrasada"};
    private static final SimpleDateFormat DATE_FORMAT_US = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss.000");       
    
    public static String getDatetimeForElasticsearch(Date date){
        String dateFormated = DATE_FORMAT_US.format(date)+"T"+TIME_FORMAT.format(date)+"Z";
        return dateFormated;
    }
    
    public static String getDatetimeNowForElasticsearch(){
        return Instant.now().toString();
    }   
    
//    private synchronized static JSONObject getDetailsDevicesFromNMap(String passwordSudo, String ip) throws Exception {
//        
//        JSONObject json = new JSONObject();
//        json.put("type", "");
//        json.put("os_details", "");
//        
//        String[] cmd = {"/bin/bash","-c","echo "+passwordSudo+"| sudo -S nmap -O --osscan-limit "+  ip};
//        
//        Process p = Runtime.getRuntime().exec(cmd);        
//        
//        if(!p.waitFor(10, TimeUnit.SECONDS)) {
//            p.destroy();
//            return json;
//        }
//                
//	BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
//    	
//        String lineOut;
//        
//	while ((lineOut = input.readLine()) != null) {
//            
//            if (lineOut.startsWith("Device type:")){
//                
//                String data = lineOut.substring(lineOut.indexOf(":")+1).trim();
//                json.put("type", data);
//                
//            }else if (lineOut.startsWith("OS details:")){
//                
//                String data = lineOut.substring(lineOut.indexOf(":")+1).trim();
//                json.put("os_details", data);
//            }
//            
//	}
//                
//	input.close();
//        p.destroy();
//        
//        return json;        
//        
//    }
    
    public static int getSecondsByPeriod(Date dStart, Date dEnd){        
        DateTime start = new DateTime(dStart);
        DateTime end = new DateTime(dEnd);                        
        int seconds = Seconds.secondsBetween(start, end).getSeconds();
        return seconds;        
    }    
    
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        
        return bd.doubleValue();
        
    }    
    
    public static Object executeScript(String script, Object value) throws Exception{
                
        Binding binding = new Binding();
        GroovyShell shell = new GroovyShell(binding);
        
        Object valueScript;
        
        if (value instanceof String){
            
            valueScript = "\""+value.toString()+"\"";
            
        }else if (value instanceof Integer || value instanceof Double || value instanceof Float || value instanceof Long){
            
            valueScript = value;
            
        }else{
            throw new Exception("only strings, integers, doubles, floats, and longs are accepted");
        
        }                                        
        
        script = script.replace("{{value}}", valueScript.toString());
        
        Object response = shell.evaluate(script);
        
        return response;        
        
    }
    
    public static String getIdDoc(String idDocActionUpdateLast, JSONObject document) throws Exception{
        
        Iterator it = document.keys();
        
        while(it.hasNext()){
            
            String key = it.next().toString();
            Object value = document.get(key);
            
            if (idDocActionUpdateLast.contains("{"+key+"}")){
                idDocActionUpdateLast = idDocActionUpdateLast.replace("{"+key+"}", (value+""));
            }            
            
        }
        
        return idDocActionUpdateLast;
        
    }
    
    public static JSONArray getErrorState(String hex) throws Exception {
        
        String mountHex = mountHex(hex);
        String oct = getOct(mountHex);        
        
        JSONArray array = new JSONArray();        
        for (int i = 0; i < oct.length() -1; i++) {            
            int bit = Integer.parseInt(oct.substring(i, i+1));            
            if (bit == 0){
                continue;
            }            
            array.put(STATUS[i]);            
        }        
        
        return array;        
    }

    private static String getOct(String hex) {

        String hex1 = hex.substring(0, 2);                
        String oct1 = Integer.toBinaryString(Integer.parseInt(hex1, 16));
        int places = 8 - oct1.length();
        for (int i = 0; i < places; i++) {
            oct1 = "0" + oct1;
        }
        
        String hex2 = hex.substring(2);
        String oct2 = Integer.toBinaryString(Integer.parseInt(hex2, 16));
        int places2 = 8 - oct2.length();
        for (int i = 0; i < places2; i++) {
            oct2 = "0" + oct2;
        }

        return oct1 + oct2;

    }

    private static String mountHex(String hex) throws Exception {

        String newHex = hex;

        if (newHex.length() > 4) {
            throw new Exception("O status não pode ter mais que 4 dígitos");
        }
        if (newHex.length() == 4) {
            return newHex;
        }
        int places = 4 - newHex.length();
        for (int i = 0; i < places; i++) {
            newHex = "0" + newHex;
        }

        return newHex;

    }
    
}