package br.com.tsinova.printerts;

import java.util.List;

public class Printer extends Host {
    
    private List<Data> listData;        

    public Printer() {
    }    

    public List<Data> getListData() {
        return listData;
    }

    public void setListData(List<Data> listData) {
        this.listData = listData;
    }    
    
    
}